<aside class="rsb">
	<div class="login">
		<?php if(is_user_logged_in()): ?>
		
			<?php  
	            global $current_user;
	            get_currentuserinfo();
	            $cur_user = wp_get_current_user();
	        ?>

			<div class="profile">
	            <p class="av_holder"><?php echo get_avatar($current_user->ID, 64); ?></p>
	            <p class="user_log"><a target=_blank href="<?php echo site_url();?>/profile/?user_name=<?php echo $cur_user->user_login; ?>" class="cur_user"><?php echo ucfirst($cur_user->user_login); ?></a></p>
	            <!-- <p class="user_log"><a href="<?php //echo admin_url('user-edit.php?user_id='.$cur_user->ID,'http');?>" class="cur_user"><?php //echo ucfirst($cur_user->user_login); ?></a></p> -->
	            <p class="logout"><a href="<?php echo site_url('logout');?>">Logout</a></p>
	        </div>

		<?php else: ?>
			<h4>Login w/ Social Media</h4><?php echo do_shortcode('[TheChamp-Login]'); ?>
		<?php endif; ?>

	</div>
	<div class="side-navigation right-nav">
		<?php sec_nav(); ?>
	</div>
	<div class="web-ads"></div>
</aside>