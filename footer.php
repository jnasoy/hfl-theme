		<!-- Footer nav -->
		<div id="footer">
			<?php footer_nav(); ?>
			<div class="clear"></div>
			<div class="copyright">
				<h5 class="reserved">&copy;<?php echo date("Y");?> <a style="color:#fff;" href="http://www.globalismedia.com" target="_blank">Globalis Media Inc.</a> All rights reserved.</h5>
				<h5 class="by_ed">Website built and maintained by <a style="color:#fff;" href="http://www.exdigita.com" target="_blank">Exdigita</a>.</h5>
			</div>
			<div class="clear"></div>
		</div>
		<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/style.js"></script>
	</div>
	<?php wp_footer(); ?>
	<!-- Don't forget analytics -->
</body>
</html>
