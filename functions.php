<?php
/**
 * HealthyFitLong Rules
 *
 *
 * @package HealthyFitLong
 * @subpackage English-Default
 * @since 2015
*/
	//initialize ob start
    add_action('init', 'do_output_buffer');
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'home-slider-size', 600, 400, true); //crops featured images or thumbnails to better fit home page slider
    function do_output_buffer(){
        ob_start();
    }
    
    //Remove the top bar
    add_filter( 'show_admin_bar', '__return_false' );
    // Add RSS links to <head> section
	automatic_feed_links();
	
	// Load jQuery
	if ( !is_admin() ) {
	   wp_deregister_script('jquery');
	   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"), false);
	   wp_enqueue_script('jquery');
	}
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
	// Declare sidebar widget zone
    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => 'Sidebar Widgets',
    		'id'   => 'sidebar-widgets',
    		'description'   => 'These are widgets for the sidebar.',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    }
    // --Main Navigation Menu--
    function main_nav(){
        $main_nav = wp_nav_menu( array(
                'container'     => false,
                'menu'          => 'main-nav',
                'menu_class'    => 'main_nav',
                'echo'          => true,
                'before'        => '',
                'after'         => '',
                'link_before'   => '',
                'link_after'    => '',
                'depth'         => 0
            ));
        return $main_nav;
    }
    //Secondary Navigation
    function sec_nav(){
        $sec_nav = wp_nav_menu( array(
                'container'     => false,
                'menu'          => 'sec-nav',
                'menu_class'    => 'sec_nav',
                'echo'          => true,
                'before'        => '',
                'after'         => '',
                'link_before'   => '',
                'link_after'    => '',
                'depth'         => 0
            ));
        return $sec_nav;
    }
    //Footer Navigation
    function footer_nav(){
        $footer_nav = wp_nav_menu( array(
                'container'     => false,
                'menu'          => 'footer-nav',
                'menu_class'    => 'footer_nav',
                'echo'          => true,
                'before'        => '',
                'after'         => '',
                'link_before'   => '',
                'link_after'    => '',
                'depth'         => 0
            ));
        return $footer_nav;
    }

?>