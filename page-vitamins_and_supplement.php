<?php
	/*
		Template Name: Vitamins and Supplements Page
	*/
?>
<?php get_header();?>
<div id="page-container" class="categ-page">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			<h2><?php the_title(); ?> - Featured Products</h2>
			
			<div class="entry" id="post-<?php the_ID(); ?>">

				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>

			</div>

		<?php endwhile; endif; ?>
	<?php echo do_shortcode('[hfl-product-vitamins]'); ?>
	<div class="clear"></div> 
	<div class="mid-nav">
		<?php sec_nav(); ?>
	</div>
	
	<div class="clear"></div>
	
</div>

<?php get_footer(); ?>