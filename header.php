<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="description" content=" Live Healthy , Live Long , Live Fit.">
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>
		<?php wp_title( '|', true, 'right' ); bloginfo('name'); ?>
		<?php  
			$disc = get_bloginfo('description','display');
			if($disc && (is_home() || is_front_page() )) echo "| $disc";
		?>
	</title>
	<link rel="shortcut icon" href="/favicon.ico">
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/rtl.css"/>
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<div id="page-wrap">

		<div id="header">
			<div class="logo">
				<h1>
					<a href="<?php echo get_option('home'); ?>/"><span id="blogname"><?php bloginfo('name'); ?></span><img src="<?php echo get_template_directory_uri();?>/images/logo.png" alt="HealthyFitLong Logo"></a>
					<span><h2><?php echo get_bloginfo('description','display'); ?></h2></span>
					<img class="hormone" src="<?php echo get_template_directory_uri();?>/images/hfl-icon.gif" alt="hormonal_change_gif">
				</h1>
				<div class="dbm_ad"></div>
			</div>
			<?php main_nav(); ?>
			<div class="clear"></div>
		</div>
		