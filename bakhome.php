<?php
	/*
		Template Name: Home Page
	*/
?>

<?php get_header(); ?>
	<div id="home-container">
		<section class="upper-content">
			<div class="random-product-left">[hfl-product-random from=0]</div>
			<div></div>
			<div class="random-product-right">[hfl-product-random from =11]</div>
		</section>
		<section class="mid-content">
			<div class="hlf-best-sellers">
				<h2 class="hlf-bestsellers-header hlf-home-blue-gradient">Best Sellers</h2>
				<div id="hlf-product-slider">
				<?php echo do_shortcode('[hfl-product-slider-slick]');?>

				</div>
			</div>
			<div class="side-navigation">
				<?php sec_nav(); ?>
			</div>
		</section>
		<section class="lower-content">
			<header class="about_header">
			<img class="about_pic" src="<?php echo get_template_directory_uri();?>/images/about.jpg" alt="about">
			<span class="about_top"><p>About</p><img src="<?php echo get_template_directory_uri();?>/images/logo.png" alt="logo"></span>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat.
			</header>
		</section>
		<section>
			<ul class="blog_main">
				<li class="blog_cont1"><div class="blog_post">Cont 1</div><a href="#"><div class="blog_read">Read More >>></div></a></li>
				<li class="blog_cont2"><div class="blog_post">Cont 2</div><a href="#"><div class="blog_read">Read More >>></div></a></li>
			</ul>
			<ul class="blog_main">
				<li class="blog_cont1"><div class="blog_post">Cont 3</div><a href="#"><div class="blog_read">Read More >>></div></a></li>
				<li class="blog_cont2"><div class="blog_post">Cont 4</div><a href="#"><div class="blog_read">Read More >>></div></a></li>
			</ul>
		</section>
			<ul class="categ_main">
				<li class="categ_holder"><a href="#"><div class="categ_img"><img src="<?php echo get_template_directory_uri();?>/images/vit_sup.jpg" alt="vitamins"></div><div class="categ_name"><h2>Vitamins and Supplements</h2></div></a></li>
				<li class="categ_holder"><div class="categ_img"><img src="<?php echo get_template_directory_uri();?>/images/skincare_pro.jpg" alt="skincare"></div><div class="categ_name"><h2>Skin Care Products</h2></div></li>
				<li class="categ_holder"><div class="categ_img"><img src="<?php echo get_template_directory_uri();?>/images/hormone_pro.jpg" alt="hormone"></div><div class="categ_name"><h2>Hormone Products</h2></div></li>
				<li class="categ_holder"><div class="categ_img"><img src="<?php echo get_template_directory_uri();?>/images/herbal_pro.jpg" alt="herbal"></div><div class="categ_name"><h2>Herbal Products</h2></div></li>
			</ul>
	</div>
	<div class="clear"></div>
<?php get_footer(); ?>
